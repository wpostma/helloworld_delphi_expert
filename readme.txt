Hello World Expert

This is a Delphi 10 Seattle IDE Expert hello-world (minimal) demo. Build it and register it in your IDE's registry key settings. See the bin folder for a sample .reg file that shows how to register it.

This demo actually does several things that are useful, and which a person starting a new wizard might find non-trivial to implement from scratch:

* It uses an ActionList in a data module which is then exposed via several mechanisms so that the user can find and run it.
* It puts a custom item in the project right click context menu.
* It also registers a global keyboard shortcut (hotkey).
* It also makes the same action visible in the IDE insight search.
* It also adds a menu to the main menu and makes the actions in that action list show up in that main menu.

No warranty is expressed or implied.  If you have questions, go read the GExperts open tools API faq. 
Don't expect free technical support from me, or anybody else, if you break your IDE, you own both pieces.

http://www.gexperts.org/open-tools-api-faq/