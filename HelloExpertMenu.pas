unit HelloExpertMenu;

// Create a Menu in the Delphi IDE's main menu area.
//
// Usage:  From some data module containing an action list:
//
//   FExpertMenus := TExpertMainMenu.Create(self);
//   FExpertMenus.ActionList := WizActions;
//   FExpertMenus.InstallMenu;

interface

uses Classes,
  SysUtils,
  Generics.Collections,
  Vcl.ActnList,
  ToolsAPI,
  Menus,
  Messages;

type

  TExpertMainMenu = class(TComponent)
  private
    FBindingIndex: Integer;
    FActionList: TActionList;
    function GetExpertMenu: TMenuItem;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure InstallMenu;
    property ActionList: TActionList read FActionList write FActionList;
  end;


implementation

uses
  Dialogs,
  Windows;

type
  EHelloExpertMenuFailure = class(Exception);

  { Register some Action menu items under the main menu }

constructor TExpertMainMenu.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

destructor TExpertMainMenu.Destroy;
begin
  inherited;
end;

procedure TExpertMainMenu.InstallMenu;
var
  i: Integer;
  Action: TAction;
  ExpertMenu: TMenuItem;
  NewItem: TMenuItem;
begin
  ExpertMenu := GetExpertMenu;
  if not Assigned(ExpertMenu) then
    exit;
  ExpertMenu.Clear;

    // Map one menu item to each action.
    for i := 0 to FActionList.ActionCount-1 do
    begin
      Action := FActionList.Actions[i] as TAction;
      NewItem := TMenuItem.Create(ExpertMenu);
      NewItem.Name := Action.Name+'_Item';
      NewItem.Caption := Action.Caption;
      NewItem.Action := Action;
      NewItem.OnClick := Action.OnExecute;
      NewItem.ShortCut := Action.ShortCut;
      NewItem.Tag := Action.Tag;
      ExpertMenu.Add(NewItem);
    end;

end;




procedure CheckAssigned(const Intf: IUnknown);
begin
  if not Assigned(Intf) then
    raise EHelloExpertMenuFailure.Create
      ('HelloExpert IDE Expert unable to access IDE Services');
end;

function IDEServices: INTAServices;
begin
  CheckAssigned(BorlandIDEServices);
  IDEServices := BorlandIDEServices as INTAServices;
  CheckAssigned(result);
end;

function TExpertMainMenu.GetExpertMenu: TMenuItem;
const
  cmnuHelloExpert = 'mnuHelloExpert';
var
  MainMenu: TMainMenu;
  i: Integer;
  toolsIndex: Integer;
begin
  result := nil;
  toolsIndex := -1;
  MainMenu := IDEServices.MainMenu;
  if not Assigned(MainMenu) then
    exit;

  for i := 0 to MainMenu.Items.Count - 1 do
  begin
    if sameText(MainMenu.Items[i].Name, 'ToolsMenu') then
    begin
      toolsIndex := i;   { Place my expert right by the Tools menu just where I like it to be }
    end;
    if sameText(MainMenu.Items[i].Name, cmnuHelloExpert) then
    begin
      result := MainMenu.Items[i];
      break;
    end;
  end;
  { My expert top level menu item doesn't exist, create it. }
  if (result = nil) and (toolsIndex > 0) then
  begin
    result := TMenuItem.Create(nil);
    result.Name := cmnuHelloExpert;
    result.Caption := '&HelloExpert';
    MainMenu.Items.Insert(toolsIndex, result);
  end
end;

end.
