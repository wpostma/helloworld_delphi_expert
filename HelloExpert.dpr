library HelloExpert;

{$R *.res}
{$IFDEF IMPLICITBUILDING This IFDEF should not be used by users}
{$ALIGN 8}
{$ASSERTIONS ON}
{$BOOLEVAL OFF}
{$DEBUGINFO ON}
{$EXTENDEDSYNTAX ON}
{$IMPORTEDDATA ON}
{$IOCHECKS ON}
{$LOCALSYMBOLS ON}
{$LONGSTRINGS ON}
{$OPENSTRINGS ON}
{$OPTIMIZATION OFF}
{$OVERFLOWCHECKS OFF}
{$RANGECHECKS OFF}
{$REFERENCEINFO ON}
{$SAFEDIVIDE OFF}
{$STACKFRAMES ON}
{$TYPEDADDRESS OFF}
{$VARSTRINGCHECKS ON}
{$WRITEABLECONST ON}
{$MINENUMSIZE 1}
{$IMAGEBASE $400000}
{$DEFINE DEBUG}
{$ENDIF IMPLICITBUILDING}
{$DESCRIPTION 'Hello Expert DLL'}
{$IMPLICITBUILD OFF}

(* NOTE: library ... uses .. exports ==> DLL.  package ... requires ... contains ==> BPL

  ToolsAPI,DesignIntf,DesignEditors resolve in this package via designide.dcp reference.

  It seems you must export INITWIZARD0001 and WizardEntryPoint to be safe.

  HelloExpertReg contains InitWizard.
*)

{$R 'ExpertResource.res' 'ExpertResource.rc'}

uses
  ToolsAPI,
  DesignIntf,
  DesignEditors,
  HelloExpertReg in 'HelloExpertReg.pas',
  HelloExpertUnit in 'HelloExpertUnit.pas',
  HelloExpertDataMod in 'HelloExpertDataMod.pas' {HelloExpertDataModule: TDataModule},
  HelloExpertKeyBinding in 'HelloExpertKeyBinding.pas',
  hweCastaliaSimplePasPar in 'castalia\hweCastaliaSimplePasPar.pas',
  hweCastaliaSimplePasParTypes in 'castalia\hweCastaliaSimplePasParTypes.pas',
  hweCastaliaPasLex in 'castalia\hweCastaliaPasLex.pas',
  hweCastaliaPasLexTypes in 'castalia\hweCastaliaPasLexTypes.pas',
  HelloExpertMenu in 'HelloExpertMenu.pas',
  HelloExpertContextMenu in 'HelloExpertContextMenu.pas';

exports
  InitWizard name ToolsAPI.WizardEntryPoint;
  //InitWizard name 'WizardEntryPoint' <-- no.
  //InitWizard name 'INITWIZARD0001' <-- correct, but better to use constant.


end.
