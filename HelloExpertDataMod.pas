unit HelloExpertDataMod;

{ This data module is to show how we would add IDE Insight
  support which requires a TActionList.

  http://stackoverflow.com/questions/10147850/add-my-own-items-to-delphi-ide-insight-f6-with-in-delphi-open-tools-api
  original author: T.Ondrej.

}

interface

uses
  System.SysUtils,
  System.Classes,
  System.Actions,
  Vcl.ActnList,
  System.ImageList,
  Vcl.ImgList, Vcl.Controls,
  {CASTALIA UNITS}


  { OUR UNITS}
  HelloExpertMenu,
  HelloExpertContextMenu,
  HelloExpertKeyBinding;

type
  THelloExpertDataModule = class(TDataModule)
    WizActions: TActionList;
    ImageList16x16: TImageList;
    ActionHelloWorld: TAction;
    procedure ActionHelloWorldExecute(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FKeyBinding: TKeyboardBinding;
    FExpertMainMenu : TExpertMainMenu;
    FProjectManagerMenu : TProjectManagerMenu;
  public
    { Public declarations }
  end;

  { manually init and cleanup from somewhere else with these functions}
procedure DMExpInit;
procedure DMExpCleanup;


var
  HelloExpertDataModule: THelloExpertDataModule;

implementation


uses
  Windows,
  ToolsAPI,
  Dialogs;

// This next bit is for Delphi XE3 and up, and tells Delphi whether this unit has an affinity to the VCL
// or to Firemonkey.
{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure THelloExpertDataModule.ActionHelloWorldExecute(Sender: TObject);
begin
{$ifdef DEBUG}
  OutputDebugString( 'Data Module Action: THelloExpertDataModule.ActionHelloWorldExecute' );
{$endif}
  ShowMessage('Hello World!');
end;

var
  Index:Integer = -1;

procedure THelloExpertDataModule.DataModuleCreate(Sender: TObject);
begin
  { Load any user customizations BEFORE you create the TKeyboardBinding.
    Or go rewrite TKeyboardBinding to make it unbind and rebind whenever you like. Your call.
   }
  FExpertMainMenu := TExpertMainMenu.Create(self);
  FExpertMainMenu.ActionList := WizActions;
  FExpertMainMenu.InstallMenu;

  FProjectManagerMenu := TProjectManagerMenu.Create(WizActions);
  FProjectManagerMenu.InstallMenu;





  FKeyBinding := TKeyboardBinding.Create( WizActions); { This is a TInterfacedObject, it is not freed explicitly }
end;

procedure THelloExpertDataModule.DataModuleDestroy(Sender: TObject);
begin
  //FKeyBinding.Free; // Don't do this. Also Warren wrote the code so you can't do what you shouldn't do. Good idea right?
  FKeyBinding := nil; // This thing is reference counted, freed when reference hits zero.


  FProjectManagerMenu.UnlinkAction;
  FProjectManagerMenu := nil; // don't free this.


  FExpertMainMenu.Free;  // Doesn't free the menu items it creates.
end;


procedure DMExpInit;
begin
  if not Assigned(HelloExpertDataModule) then
  begin
    HelloExpertDataModule := THelloExpertDataModule.Create(nil); // This isn't a TComponent sadly.
    Index := (BorlandIDEServices as INTAIDEInsightService).AddActionList(HelloExpertDataModule.WizActions);
  end;

end;

procedure DMExpCleanup;
begin
 if Index<> -1 then
    (BorlandIDEServices as INTAIDEInsightService).RemoveActionList(Index);
 Index := -1;
 if Assigned(HelloExpertDataModule) then
 begin
      HelloExpertDataModule.Free;
      HelloExpertDataModule := nil;
 end;
end;



//initialization
//  Index := -1;
//
end.
