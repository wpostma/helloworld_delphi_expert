// Expert Setup/Registration

unit HelloExpertReg;

interface

uses ToolsAPI,
  Windows,
  Vcl.Graphics,
  SysUtils,
  Vcl.Dialogs,
  HelloExpertMenu,
  HelloExpertUnit;

// TWizardInitProc = function(const BorlandIDEServices: IBorlandIDEServices;
// RegisterProc: TWizardRegisterProc;
// var Terminate: TWizardTerminateProc): Boolean stdcall;

function InitWizard(const BorlandIDEServices: IBorlandIDEServices;
  RegisterProc: TWizardRegisterProc; var Terminate: TWizardTerminateProc)
  : Boolean stdcall;

(* In really old delphi versions only $R abc.res syntax is allowed but now we can compile like this *)
{$R ExpertResource.rc ExpertResource.res}
// Register is only applicable in a BPL. In this case, we must export this function from exports
// (see the .dpr source for the exports section).

var
  SplashImage: TBitmap;

implementation

function InitWizard2(const BorlandIDEServices: IBorlandIDEServices): IOTAWizard;
var
  Expert: THelloExpert;
begin
  Expert := THelloExpert.Create;
  Result := Expert;

  { wizard should have a splash icon and show its title/description strings on
    the IDE startup splash screen }
  try
    SplashImage := Vcl.Graphics.TBitmap.Create;
    SplashImage.LoadFromResourceName(HInstance, 'ExpertLogo');

    {If you don't want to show an icon when loading then omit this next line}
    SplashScreenServices.AddPluginBitmap(Expert.SplashTitleString,SplashImage.Handle);

    {oddly enough the image doesn't seem to do anything here:}
    (BorlandIDEServices as IOTAAboutBoxServices)
      .AddPluginInfo( { title } Expert.SplashTitleString,
      Expert.SplashDescriptionString, SplashImage.Handle);

  except
    on E: Exception do
    begin
      MessageDlg('Failed to load wizard splash image', mtError, [mbOK], 0);
      OutputDebugString('Failed to load splash image');
    end;
  end;
end;

function InitWizard(const BorlandIDEServices: IBorlandIDEServices;
  RegisterProc: TWizardRegisterProc; var Terminate: TWizardTerminateProc)
  : Boolean stdcall;
var
  AWizard: IOTAWizard;
begin
  try
    // RegisterPackageWizard(THelloExpert.Create); // BPL style registration. Don't do this.
    AWizard := InitWizard2(BorlandIDEServices);
    if Assigned(AWizard) then
    // better to not install a wizard we couldn't create, than to crash.
    begin
      RegisterProc(AWizard);
      Result := True;
    end
    else
      Result := False;



  except
    on E: Exception do
    begin
      MessageDlg('Failed to load wizard. internal failure:' + E.ClassName + ':'
        + E.Message, mtError, [mbOK], 0);
      Result := False;
    end;

  end;
end;

initialization

//
finalization

FreeAndNil(SplashImage);


end.
