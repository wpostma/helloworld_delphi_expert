object HelloExpertDataModule: THelloExpertDataModule
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 150
  Width = 215
  object WizActions: TActionList
    Images = ImageList16x16
    Left = 40
    Top = 32
    object ActionHelloWorld: TAction
      Category = 'Experts'
      Caption = 'Hello World'
      Hint = 'This is the Hello Expert action hint message'
      ShortCut = 24655
      OnExecute = ActionHelloWorldExecute
    end
  end
  object ImageList16x16: TImageList
    Left = 128
    Top = 32
  end
end
