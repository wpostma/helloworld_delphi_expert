// LEARNING EXERCISE:
//  Compare and contrast HelloExpertUnit and HelloWizardUnit.

unit HelloExpertUnit;

interface

uses ToolsAPI;

type
  // TNotifierObject has stub implementations for the necessary but
  // unused IOTANotifer methods
  THelloExpert = class(TNotifierObject, IOTAWizard) // IOTAMenuWizard not useful here as we don't want a menu item in the Help menu.
  public
	// IOTAWizard interafce methods(required for all wizards/experts)
	function GetIDString: string;
	function GetName: string;
	function GetState: TWizardState;
	procedure Execute;

  procedure AfterConstruction; override;
  procedure BeforeDestruction; override;

	// IOTAMenuWizard (creates a simple menu item on the help menu)
	function GetMenuText: string;

  // HelloExpertReg calls this function to get the delphi splash title.
  function SplashTitleString: string;
  // And this one to get a description;
  function SplashDescriptionString: string;

  end;


implementation

uses HelloExpertDataMod,
     Dialogs;

procedure THelloExpert.AfterConstruction;
begin
  inherited;
  HelloExpertDataMod.DMExpInit; { creates key bindings, IDE Insight actionlist, etc, }
end;

procedure THelloExpert.BeforeDestruction;
begin
  inherited;
  HelloExpertDataMod.DMExpCleanup; { destroys key bindings, actionlist, etc }
end;

procedure THelloExpert.Execute;
begin
  ShowMessage('Hello World!');
end;

function THelloExpert.GetIDString: string;
begin
  Result := 'EB.HelloWizard';
end;

function THelloExpert.GetMenuText: string;
begin
  Result := '&Hello Wizard';
end;

function THelloExpert.GetName: string;
begin
  Result := 'Hello Wizard';
end;

function THelloExpert.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;

function THelloExpert.SplashDescriptionString: string;
begin
  Result := 'Written by Warren Postma';
end;

function THelloExpert.SplashTitleString: string;
begin
  Result := 'HelloWorld Expert 0.1';
end;

end.

